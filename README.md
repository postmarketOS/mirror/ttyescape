# TTYescape

TTYescape is a collection of config files and a shell script enabling mobile
users to escape the mortal limitations of "Desktop Environments" and "hardware
acceleration", ascending to the one TRUE form of computer usage - the TTY.

See also: https://wiki.postmarketos.org/wiki/TTYescape
